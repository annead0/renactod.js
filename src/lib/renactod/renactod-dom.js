// file: src/qnd-react-dom.js
import * as snabbdom from 'snabbdom';
import propsModule from 'snabbdom/modules/props';
import eventlistenersModule from 'snabbdom/modules/eventlisteners';
import renactod from './renactod';

// propsModule -> this helps in patching text attributes
// eventlistenersModule -> this helps in patching event attributes
const reconcile = snabbdom.init([propsModule, eventlistenersModule]);
// we need to maintain the latest rootVNode returned by render
const render = (el, rootDomElement) => {
  reconcile(rootDomElement,el);
}

// Q

// to be exported like ReactDom.render
const RenactodDOM = {
  render
};

export default RenactodDOM;